package com.hem.hello;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout ll_register = (LinearLayout) findViewById(R.id.ll_register);
        LinearLayout ll_login = (LinearLayout) findViewById(R.id.ll_login);

        ll_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("Register clicked");
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

        ll_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("Navbar Clicked");
                startActivity(new Intent(getApplicationContext(), NavBarActivity.class));
            }
        });

        findViewById(R.id.ll_pref).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("Pref Clicked");
                startActivity(new Intent(getApplicationContext(), PreferenceActivity.class));
            }
        });


        findViewById(R.id.ll_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("List clicked");
                startActivity(new Intent(getApplicationContext(), ListStudentActivity.class));
            }
        });

        findViewById(R.id.api).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("API List clicked");
                startActivity(new Intent(getApplicationContext(), APIListActivity.class));
            }
        });
        findViewById(R.id.tabs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toast("Tabs clicked");
                startActivity(new Intent(getApplicationContext(), TabsActivityActivity.class));
            }
        });

    }

    private void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

}