package com.hem.hello;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by hereshem on 7/28/16.
 */
public class JokesAdapter extends RecyclerView.Adapter<JokesAdapter.JokeViewHolder>{

    List<Students> list;

    public JokesAdapter(List<Students> list) {
        Log.i("Logs", "Constructor JokesAdapter");
        this.list = list;
    }

    @Override
    public JokeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("Logs", "onCreateViewHolder called");
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list, parent, false);
        return new JokeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(JokeViewHolder holder, int position) {
        //holder.image
        Log.i("Logs", "onBindVieHolder position " + position);
        holder.title.setText(list.get(position).getEmail());
    }

    @Override
    public int getItemCount() {
        Log.i("Logs", "Getcount called with size " + list.size());
        return list.size();
    }

    class JokeViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        ImageView image;

        public JokeViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.email);
            image = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }

}
