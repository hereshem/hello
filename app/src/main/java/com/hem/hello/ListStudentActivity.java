package com.hem.hello;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ListStudentActivity extends AppCompatActivity {

    ListView lv;

    List<Students> list;

    MyDbHelper db;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_student);

        lv = (ListView) findViewById(R.id.listView);
        db = new MyDbHelper(this);

        list = db.getStudents();

        adapter = new ArrayAdapter(getApplicationContext(), R.layout.row_list, list){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = getLayoutInflater().inflate(R.layout.row_list, null);

                Students s = list.get(position);

                TextView tv = (TextView) v.findViewById(R.id.email);
                tv.setText(s.getEmail());

                TextView tname = (TextView) v.findViewById(R.id.name);
                tname.setText(s.getName());

                return v;

                //return super.getView(position, convertView, parent);
            }
        };

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListStudentActivity.this, "You clicked " + list.get(position).getName(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
