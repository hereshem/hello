package com.hem.hello;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class APIListFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_recycler, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callNetwork();
    }

    private void callNetwork() {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>(){

            @Override
            protected String doInBackground(Void... params) {
                ServerRequest req = new ServerRequest();
//                String res = req.httpGetData("http://jokesanjal.com/apis/jokes?cat_id=1");
                String res = req.httpGetData("http://api.hemshrestha.com.np/?action=list");
                return res;
            }

            @Override
            protected void onPostExecute(String abcdefhjgdfj) {
                super.onPostExecute(abcdefhjgdfj);
                //Toast.makeText(getActivity(), abcdefhjgdfj, Toast.LENGTH_SHORT).show();
                parseJSON(abcdefhjgdfj);
            }
        };
        task.execute();
    }

    private void parseJSON(String abcdefhjgdfj) {
        try {
            JSONObject jObj = new JSONObject(abcdefhjgdfj);
//            JSONArray jsonArray = new JSONArray(abcdefhjgdfj);
            JSONArray jsonArray = jObj.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject j = jsonArray.getJSONObject(i);
                String n = j.getString("c_fname");
//                String e = j.getString("n_home");
//                String p = j.getString("n_mobile");
                String a = j.getString("n_mobile");

                Students s = new Students(n,"","",a);
                list.add(s);
            }

            showListView();


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error in JSON", Toast.LENGTH_SHORT).show();
        }
    }

    private void showListView() {

        JokesAdapter adapter = new JokesAdapter(list);

        RecyclerView lv = (RecyclerView) getView().findViewById(R.id.recycler);
        lv.setLayoutManager(new LinearLayoutManager(getActivity()));

        lv.setAdapter(adapter);

    }

    List<Students> list = new ArrayList<>();



}
