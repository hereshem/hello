package com.hem.hello;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class APIListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apilist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        callNetwork();
    }

    private void callNetwork() {
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>(){

            @Override
            protected String doInBackground(Void... params) {
                ServerRequest req = new ServerRequest();
                String res = req.httpGetData("http://jokesanjal.com/apis/jokes?cat_id=1");
                return res;
            }

            @Override
            protected void onPostExecute(String abcdefhjgdfj) {
                super.onPostExecute(abcdefhjgdfj);
                Toast.makeText(APIListActivity.this, abcdefhjgdfj, Toast.LENGTH_SHORT).show();
                parseJSON(abcdefhjgdfj);
            }
        };
        task.execute();
    }

    private void parseJSON(String abcdefhjgdfj) {
        try {
            //JSONObject jObj = new JSONObject(abcdefhjgdfj);
            JSONArray jsonArray = new JSONArray(abcdefhjgdfj);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject j = jsonArray.getJSONObject(i);
                String n = j.getString("description");
//                String e = j.getString("n_home");
//                String p = j.getString("n_mobile");
                String a = j.getString("image");

                Students s = new Students(n,"","",a);
                list.add(s);
            }

            showListView();


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(APIListActivity.this, "Error in JSON", Toast.LENGTH_SHORT).show();
        }
    }

    private void showListView() {

        ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(), R.layout.row_list, list){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                Log.i("Logs", "Loaded position " + position);
                View v = getLayoutInflater().inflate(R.layout.row_list, null);

                if(position%2 == 0){
                    v.setBackgroundColor(Color.LTGRAY);
                }


                Students s = list.get(position);

                ImageView iv = (ImageView) v.findViewById(R.id.imageView);

                if(!s.getAddress().isEmpty()) {
                    Picasso.with(getApplicationContext()).load(s.getAddress()).into(iv);
                }
                TextView tv = (TextView) v.findViewById(R.id.email);
                tv.setText(s.getName());

//                TextView tname = (TextView) v.findViewById(R.id.name);
//                tname.setText(s.getName());

                return v;

                //return super.getView(position, convertView, parent);
            }
        };


        ListView lv = (ListView) findViewById(R.id.listView);

        lv.setAdapter(adapter);

    }

    List<Students> list = new ArrayList<>();



}
