package com.hem.hello;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class PreferenceActivity extends AppCompatActivity {

    CheckBox checkbox;
    EditText editText;
    Button button;

    SharedPreferences pf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);

        editText = (EditText) findViewById(R.id.editText);
        checkbox = (CheckBox) findViewById(R.id.checkBox);
        button = (Button) findViewById(R.id.button);

        pf = PreferenceManager.getDefaultSharedPreferences(this);

        editText.setText(pf.getString("name", "Default Value"));
        checkbox.setChecked(pf.getBoolean("checked", false));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = editText.getText().toString();
                boolean checked = checkbox.isChecked();

                Toast.makeText(PreferenceActivity.this, "Text = " + text + " and checked = " + checked, Toast.LENGTH_SHORT).show();

                pf.edit()
                        .putString("name", text)
                        .putBoolean("checked", checked)
                        .apply();

            }
        });




    }
}
