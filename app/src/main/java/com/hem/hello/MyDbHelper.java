package com.hem.hello;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hereshem on 7/7/16.
 */
public class MyDbHelper extends SQLiteOpenHelper {

    String TABLE_STUDENT = "student";

    public MyDbHelper(Context context) {
        super(context, "student", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_STUDENT + " ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name VARCHAR(50), " +
                "address VARCHAR(250), " +
                "phone VARCHAR(50), " +
                "email VARCHAR(50)" +
                ")";

        Log.i("db", sql);
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //backup data
        String sql = "DROP TABLE IF EXISTS " + TABLE_STUDENT;
        db.execSQL(sql);
        onCreate(db);
        //restore data
    }


    // CRUD

    public void saveStudent(Students s){

        String sql = "INSERT INTO " + TABLE_STUDENT + " (name, address, phone, email) VALUES (" +
                "'" + s.name + "', "+
                "'" + s.address + "', "+
                "'" + s.phone + "', "+
                "'" + s.email + "'"+
                ")";

        Log.i("db", sql);
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(sql);
        db.close();

    }

    public List<Students> getStudents(){
        List<Students> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * from " + TABLE_STUDENT;
        Cursor cursor = db.rawQuery(sql, null);

        if(cursor.moveToFirst()){
            do{
                String n = cursor.getString(1);
                String a = cursor.getString(2);
                String p = cursor.getString(3);
                String e = cursor.getString(4);

                Students s = new Students(n,a,p,e);
                list.add(s);

            }while (cursor.moveToNext());
        }
        return list;
    }


}
