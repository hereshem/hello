package com.hem.hello;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    EditText name, email, address, phone;
    Button button;
    MyDbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        phone = (EditText) findViewById(R.id.phone);
        address = (EditText) findViewById(R.id.address);
        button = (Button) findViewById(R.id.button);

        db = new MyDbHelper(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String n = name.getText().toString();
                String e = email.getText().toString();
                String p = phone.getText().toString();
                String a = address.getText().toString();

                Students s = new Students(n, e, p, a);
                db.saveStudent(s);
                Toast.makeText(RegisterActivity.this, "Student saved", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
